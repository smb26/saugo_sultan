// NOTE: all the console logging statements are for testing purposes

var pictureIDs = [/* will store the number of the picture that is between 0 & 2112 */];
var cachedImages = {};
/* the purpose of this function is to make the first call 
to the xkcd.com website and retrive the 1st and 2nd pictures
The first picture is going to be loaded immediately while the
the second picture will be in the 'background' waiting for the user
to click next. This will make the second picture loads */
function firstRetrieval() {
    // this random number is generated, it will be between 1 and 2111
    // the reason is: the api has 2111 resources
    var random_1 = Math.floor(Math.random() * 2112);
    pictureIDs.push(random_1);
    axios.get('https://xkcd.now.sh/' + random_1, { headers: { 'Access-Control-Allow-Origin': '*' } })
        .then(function (response) {
            console.log(response.data.title);
            var date = new Date();
            // creating the date to be in the form "Day Month Year"
            var ddmmyy = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
            console.log(ddmmyy);

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ":" + minutes +":"+seconds;
            console.log(time);
            /* creating image tag, setting the src attribute, and setting the ID.
                Finally, appending it inside a div*/
            var img = document.createElement("img");
            img.src = "" + response.data.img;
            img.setAttribute("id", "showing");
            img.setAttribute("title", response.data.title);
            var container = document.getElementById("displayed-image");
            container.appendChild(img);

            var innerLi = document.createElement("li");
            innerLi.innerHTML = "" + ddmmyy + " " + time;
            var ul = document.createElement("ul");
            var aTag = document.createElement("a");
            aTag.innerHTML = response.data.title;
            aTag.setAttribute("href", "javascript:;");
            var outerLi = document.createElement("li");
            //outerLi.setAttribute("onclick", "getPreviouslySeenImage(this)");
            // the above statement or the one below are the same, it is just a matter of preference
            outerLi.addEventListener("click", function () { getPreviouslySeenImage(this) });
            outerLi.setAttribute("id", random_1);
            var list = document.getElementById("list");
            outerLi.appendChild(aTag);
            ul.appendChild(innerLi);
            outerLi.appendChild(ul);
            list.appendChild(outerLi);
            // the second random number is for the second picture that will be retrieved
            var random_2 = Math.floor(Math.random() * 2112);
            // if we already have this number in our picture IDs array, then that means we have displayed it before
            while (pictureIDs.includes(random_2)) {
                // we try to give another value to random_2
                random_2 = Math.floor(Math.random() * 2112);
            }
            axios.get('https://xkcd.now.sh/' + random_2, { headers: { 'Access-Control-Allow-Origin': '*' } })
                .then(function (response) {
                    var img2 = document.createElement("img");
                    img2.src = "" + response.data.img;
                    img2.setAttribute("id", "no-display");
                    img2.setAttribute("title", response.data.title);
                    img2.setAttribute("number", random_2);
                    var container2 = document.getElementById("displayed-image");
                    container2.appendChild(img2);
                })
                .catch(function (error) {
                    console.log(error);
                    errorImage("no-display");
                });
        })
        .catch(function (error) {
            console.log(error);
            errorImage("showing");
        });
}

function nextRetrieval() {
    /* What is going on is the following:
    The image that was being shown will be removed from the DOM.
    Then the image that was hiding behind (has property of display: none)
    will be shown. this will make it look like it is loading fast 
    Also, a new time stamp will be generated for the newly displayed picture*/
    document.getElementById("showing").remove();
    document.getElementById("no-display").setAttribute("id", "showing");

    if (! (document.getElementById("showing").getAttribute("title") == "Error")) {
        var date = new Date();
        // creating the date to be in the form "Day Month Year"
        var ddmmyy = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
        console.log(ddmmyy);

        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ":" + minutes+":"+seconds;
        console.log(time);

        var innerLi = document.createElement("li");
        innerLi.innerHTML = "" + ddmmyy + " " + time;
        var ul = document.createElement("ul");
        var aTag = document.createElement("a");
        aTag.innerHTML = document.getElementById("showing").getAttribute("title");
        aTag.setAttribute("href", "javascript:;");
        var outerLi = document.createElement("li");
        //outerLi.setAttribute("onclick", "getPreviouslySeenImage(this)");
        // the above statement or the one below are the same, it is just a matter of preference
        outerLi.addEventListener("click", function () { getPreviouslySeenImage(this) });
        outerLi.setAttribute("id", document.getElementById("showing").getAttribute("number"));
        var list = document.getElementById("list");
        outerLi.appendChild(aTag);
        ul.appendChild(innerLi);
        outerLi.appendChild(ul);
        list.appendChild(outerLi);
    }else{
        console.log(document.getElementById("showing").getAttribute("title"));
    }

    /* Now that we have wiped out one picture, we will make a call to retrieve a
        picture and hide it in the background */
    var random = Math.floor(Math.random() * 2112);
    // if we already have this number in our picture IDs array, then that means we have displayed it before
    while (pictureIDs.includes(random)) {
        // we try to give another value to random_2
        random = Math.floor(Math.random() * 2112);
    }
    axios.get('https://xkcd.now.sh/' + random, { headers: { 'Access-Control-Allow-Origin': '*' } })
        .then(function (response) {
            /* If speed wasn't required/desired then one way to eliminate the call to next() which then calls for waitForElements
                and provide it with nextRetrieval as callback parameter.
                Then: 
                The element with the ID "no-display" is modified inside the axios call
                If you do this statement before the axios call, then you will have an error.
                The reason is the following: If you click NEXT button in a fast manner (in the HTML document), while
                also having onclick=nextRetrieval() instead of onclick=next,
                you try to change the ID of "no-display" to "showing", then such element might not be existing YET
                because it relies on the response from XKCD.com to exist */

            console.log(response.data);
            var img2 = document.createElement("img");
            img2.src = "" + response.data.img;
            img2.setAttribute("id", "no-display");
            img2.setAttribute("title", response.data.title);
            img2.setAttribute("number", random);
            var container2 = document.getElementById("displayed-image");
            container2.appendChild(img2);
        })
        .catch(function (error) {
            console.log(error);
        });

}

function next() {
    waitForElements(nextRetrieval);
}

function waitForElements(callback) {
    if (!(document.getElementById("no-display") && document.getElementById("showing"))) {
        console.log('waiting');
        console.log(document.getElementById("no-display"));
        console.log(document.getElementById("showing"));
        window.setTimeout(waitForElements.bind(null, callback), 100); /* this checks the flag every 100 milliseconds*/
    } else {
        console.log('done');
        callback();
    }
}

function getPreviouslySeenImage(element) {
    /*For testing purposes
    alert(element.getElementsByTagName('a')[0].innerText);
    alert(element.id);*/

    axios.get('https://xkcd.now.sh/' + element.id, { headers: { 'Access-Control-Allow-Origin': '*' } })
        .then(function (response) {
            document.getElementById("showing").remove();
            var date = new Date();
            // creating the date to be in the form "Day Month Year"
            var ddmmyy = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ":" + minutes+":"+seconds;
            /* creating image tag, setting the src attribute, and setting the ID.
                Finally, appending it inside a div*/
            var img = document.createElement("img");
            img.src = "" + response.data.img;
            img.setAttribute("id", "showing");
            img.setAttribute("title", response.data.title);
            var container = document.getElementById("displayed-image");
            container.appendChild(img);


            var innerLi = document.createElement("li");
            innerLi.innerHTML = "" + ddmmyy + " " + time;
            var list = document.getElementById(element.id);
            var ul = list.getElementsByTagName('ul')[0];
            ul.appendChild(innerLi);
        })
        .catch(function (error) {
            console.log(error);
        });
}

function errorImage(text) {
    var img = document.createElement("img");
    img.src = "assets/21288241.jpg";
    img.setAttribute("id", text);
    img.setAttribute("title", "Error");
    var container = document.getElementById("displayed-image");
    container.appendChild(img);
}